from random import randint

# initial variables
sticks_remaining = 20
turn = 1


# prints the game board
def print_board(remaining):
    for x in range(5):
        print("| " * remaining)


# checks whose turn it is and calls remove_sticks()
def turn_check(turn, remaining):
    remove = 0
    if(turn == 1):
        remove = input("How many sticks would you like to remove?")
        remaining = remove_sticks(remove, remaining, turn)
        turn = 2
        return(turn, remaining)
    else:
        if(remaining < 4):
            remove = remaining - 1
            remaining = remove_sticks(remove, remaining, turn)
        else:
            remove = randint(1, 3)
            remaining = remove_sticks(remove, remaining, turn)
        turn = 1
        return(turn, remaining)


# removes the selected sticks, returns the remaining
def remove_sticks(n, remaining, turn):
    remaining -= n
    print("Player" + str(turn) + " took " + str(n) + " sticks!")
    return(remaining)


# checks if game is over, returns boolean for game loop
def check_win(remaining, turn):
    if(remaining < 2):
        if(turn == 1):
            print("Player2 wins!")
            return(True)
        else:
            print("Player1 wins!")
            return(True)
    return(False)


# game loop, runs the game
while(check_win(sticks_remaining, turn) != True):
    print_board(sticks_remaining)
    turn, sticks_remaining = turn_check(turn, sticks_remaining)
